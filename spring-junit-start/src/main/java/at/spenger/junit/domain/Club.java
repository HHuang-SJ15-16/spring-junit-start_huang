package at.spenger.junit.domain;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import at.spenger.junit.domain.Person.Sex;

public class Club {
	private List<Person> l;

	public Club() {
		l = new ArrayList<>();
	}

	public boolean enter(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		return l.add(p);
	}

	public int numberOf() {
		return l.size();
	}

	public List<Person> getPersons() {
		return Collections.unmodifiableList(l);
	}

	public int avgAge() {
		int total = 0;
		for (Person p : l) {
			int year = LocalDate.now().getYear();
			total = total + (year - p.getBirthday().getYear());
		}
		return total / l.size();
	}

	public void sort() {
		l.sort(new Comparator<Person>() {

			@Override
			public int compare(Person arg0, Person arg1) {
				if (arg0.getBirthday().getYear() > arg1.getBirthday().getYear()) {
					return 1;
				}
				return 0;
			}

		});
	}

	public void delete(String lastname, String firstname) {
		for (Person p : l) {
			if (p.getFirstName() == firstname && p.getLastName() == lastname) {
				l.remove(p);
				break;
			}
		}
	}

	public int totalMember() {
		return l.size();
	}

	public String totalMenWomen() {
		int anzMen = 0;
		int anzWomen = 0;
		for (Person p : l) {
			if (p.getSex() == Sex.MALE) {
				anzMen++;
			}
			if (p.getSex() == Sex.FEMALE) {
				anzWomen++;
			}
		}
		String result = "The total number of male number is: " + anzMen
				+ "The total number of female number is: " + anzWomen;
		return result;
	}

	public void sortNameAndAge() {
		l = l.stream()
				.sorted((x, y) -> y.getLastName().compareTo(x.getLastName()))
				.collect(Collectors.toList());
	}

	public double avgage() {
		double avgage = l.stream().mapToInt(Person::getAge).average()
				.getAsDouble();
		return avgage;
	}

	public void printMale() {
		l.stream().filter(e -> e.getSex() == Person.Sex.MALE)
				.forEach(e -> System.out.println(e.getFirstName()));
	}

	public void printAll() {
		l.stream().forEach(
				e -> System.out.println(e.getFirstName() + " "
						+ e.getLastName()));
	}

	public void remove(String Lastname, String Firstname) {
		l.remove(l
				.stream()
				.findFirst()
				.filter(e -> e.getFirstName() == Firstname
						&& e.getLastName() == Lastname));
	}
}
